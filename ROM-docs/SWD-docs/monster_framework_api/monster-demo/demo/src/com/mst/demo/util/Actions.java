package com.mst.demo.util;

import java.lang.reflect.Constructor;

import com.mst.demo.material.FloatingActionButtonFragment;
import com.mst.demo.material.RecyclerViewFragment;
import com.mst.demo.material.SliderLayoutFragment;
import com.mst.demo.material.TabFragment;
import com.mst.demo.material.ToolbarFragment;

import android.app.Activity;
import android.app.Fragment;
import android.util.ArrayMap;
import android.util.Log;

public class Actions {
	
	private Actions(){
		
	}
	
	private static final String MATERIAL_WIDGET_PKG_NAME = "com.mst.demo.material.";
	
	public static final String ACTION_FOR_DEMO_PREFERENCE = "com.mst.demo.ACTION_PREFRENCE_DEMO";
	
	public static final String MATERIAL_WIDGET_ITEMS_NAME = "name";
	
	public static final String MATERIAL_WIDGET_ITEMS_POSITION = "position";
	
	public static String[] MATERIAL_WIDGET = {
			"Toolbar","FloatingActionButton"
			,"SliderLayout"
			,"RecyclerView","Tab"
			,"NumberPicker"
			,"TimePicker"
			,"DatePicker"
			,"Menu","Spinner","CompoundButton"/*,"Ripple Animation"
			,"Transition"*/};
	
	
	
	public static void switchToFragment(Activity activity,int position){
		FragmentUtils u = new FragmentUtils(activity);
		String fragmentName = FragmentUtils.ENTRY_FRAGMENTS[position];
		if(u.isValidFragment(fragmentName)){
			u.switchToFragment(fragmentName, null, false, false, 0, MATERIAL_WIDGET[position], true);
		}
		
		
	}
	
	
}
