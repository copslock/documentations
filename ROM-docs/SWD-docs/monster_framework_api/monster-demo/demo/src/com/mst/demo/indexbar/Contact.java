package com.mst.demo.indexbar;

import java.util.ArrayList;

/**
 * Created by caizhongting on 16-8-9.
 */
public class Contact {
    public String name;
    public String pinyin;
    public int type = 0;
    public ArrayList<String> firstLetter;
    public Contact(){
        firstLetter = new ArrayList<>();
    }
}
