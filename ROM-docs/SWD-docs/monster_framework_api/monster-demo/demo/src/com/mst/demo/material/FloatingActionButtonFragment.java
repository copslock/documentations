package com.mst.demo.material;

import mst.widget.FloatingActionButton;
import mst.widget.FloatingActionButton.OnFloatActionButtonClickListener;
import mst.widget.toolbar.Toolbar;
import mst.widget.toolbar.Toolbar.OnMenuItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mst.demo.R;
public class FloatingActionButtonFragment extends Fragment{
	private static final String 	TAG = "FloatingActionButtonFragment";
	private FloatingActionButton mFloatingButton;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_floating_actionbutton, container,false);
		mFloatingButton = (FloatingActionButton)view.findViewById(R.id.floating_action_button);
		mFloatingButton.setOnFloatingActionButtonClickListener(new OnFloatActionButtonClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "Clicked FloatingActionButton", Toast.LENGTH_SHORT).show();
			}
		});
		return view;
	}

	
	

}
