package com.mst.demo.indexbar;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mst.demo.R;

import java.util.ArrayList;

/**
 * Created by caizhongting on 16-8-9.
 */
public class ContactAdapter extends BaseAdapter {
    private ArrayList<Contact> mData;
    private Context mContext;

    public ContactAdapter(Context context){
        mContext = context;
    }
    @Override
    public int getCount() {
        if(mData != null){
            return mData.size();
        }
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        Contact contact = mData.get(position);
        return contact.type;
    }

    @Override
    public Object getItem(int position) {
        if(mData != null && position >= 0 && position < getCount()){
            return mData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contact contact = mData.get(position);
        ViewHolder viewHolder = null;
        ViewHolder1 viewHolder1 = null;
        int type = getItemViewType(position);

        if (convertView == null) {
            if(type == 0) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.contact_item_slider, parent, false);
                viewHolder.name = (TextView) convertView.findViewById(R.id.contact_name);
                viewHolder.place = (TextView) convertView.findViewById(R.id.call_place);
                viewHolder.time = (TextView) convertView.findViewById(R.id.record_time);
                viewHolder.record = (ImageView) convertView.findViewById(R.id.call_record);
                viewHolder.card = (ImageView) convertView.findViewById(R.id.call_card);
                convertView.setTag(viewHolder);
            }else if(type == 1){
                viewHolder1 = new ViewHolder1();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.contact_group, parent, false);
                viewHolder1.name = (TextView) convertView.findViewById(R.id.group_name);
                convertView.setTag(viewHolder1);
            }
        } else {
            if(type == 0) {
                viewHolder = (ViewHolder) convertView.getTag();
            }else if(type == 1){
                viewHolder1 = (ViewHolder1) convertView.getTag();
            }
        }

        if(type == 0) {
            viewHolder.name.setText(contact.name);
            viewHolder.place.setText("深圳");
            viewHolder.time.setText("一小时前");
        }else if(type == 1){
            viewHolder1.name.setText(contact.name);
        }

        return convertView;
    }

    public void setData(ArrayList<Contact> data){
        mData = data;
    }

    public static class ViewHolder{
        public TextView name;
        public TextView place;
        public TextView time;
        public ImageView record;
        public ImageView card;
    }

    public static class ViewHolder1{
        public TextView name;
    }
}
