package com.mst.demo.material;

import java.util.ArrayList;
import java.util.List;

import com.mst.demo.AgendaFragment;
import com.mst.demo.FragmentAdapter;
import com.mst.demo.InfoDetailsFragment;
import com.mst.demo.ShareFragment;

import mst.widget.DrawerLayout;
import mst.widget.NavigationView;
import mst.widget.TimePicker;
import mst.widget.TimePicker.OnTimeChangedListener;
import mst.widget.ViewPager;
import mst.widget.tab.TabLayout;
import mst.widget.toolbar.Toolbar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mst.demo.R;
public class TimePickerFragment extends Fragment {
	private TimePicker mTimePicker;
	private TextView mResult;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_time_picker, container,false);
		initView(view);
		return view;
	}
	

    private void initView(View view) {
        mTimePicker = (TimePicker)view.findViewById(R.id.time_picker);
        //如果需要显示24小时格式的时间就设置为true,
        //如果需要显示12小时格式的时间就设置为false
        //默认是显示12小时格式的
        mTimePicker.setIs24HourView(true);
        mResult = (TextView) view.findViewById(android.R.id.text1);
        //获取当前时间
        mResult.setText(mTimePicker.getHour()+":"+mTimePicker.getMinute());
        //设置滑动时时间变化的监听
        mTimePicker.setOnTimeChangedListener(new OnTimeChangedListener() {
			
			@Override
			public void onTimeChanged(TimePicker picker, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				mResult.setText(hourOfDay+":"+minute);
			}
		});
    }
    
    

}
