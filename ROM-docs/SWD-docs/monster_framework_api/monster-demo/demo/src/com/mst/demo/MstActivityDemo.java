package com.mst.demo;

import java.util.ArrayList;

import com.mst.demo.util.Actions;

import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import mst.app.MstActivity;
import mst.view.menu.bottomnavigation.BottomNavigationView;
import mst.widget.ActionMode;
import mst.widget.ActionModeListener;
import mst.widget.MstListView;
import mst.widget.ActionMode.Item;

import com.mst.demo.R;
public class MstActivityDemo extends MstActivity implements OnItemClickListener
,OnItemLongClickListener{

	private MstListView mListView;
	private String[] mItems;
	private MyListAdapter mAdapter;
	private boolean mEditMode = false;
	private boolean mSelectAll = false;
	private BottomNavigationView mBottombar;
	private ArrayMap<Integer,Boolean> mSelectedItems = new ArrayMap<Integer,Boolean>();
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		//设置视图给Activity的时候请调用这个方法，如果不需要显示Toolbar
		//的话，请在主题中将com.mst:windowHasToolbar设置为false，
		setMstContentView(R.layout.mst_activity_demo);
		mListView = (MstListView) findViewById(android.R.id.list);
		mBottombar =(BottomNavigationView)findViewById(R.id.bottom_menu);
		getListView().setOnItemClickListener(this);
		getListView().setOnItemLongClickListener(this);
		
	}
	
	
	@Override
	protected void initialUI(Bundle savedInstanceState) {
		/*
		 * 在这里加载初始化时需要的一些数据，这些数据在Activity启动之后会
		 * 更新显示在UI上。
		 */
		mItems = getResources().getStringArray(R.array.list_test_array);
		for(int i= 0;i< mItems.length;i++){
			mSelectedItems.put(i, false);
		}
		mAdapter = new MyListAdapter();
		setListAdapter(mAdapter);
		inflateToolbarMenu(R.menu.toolbar_menu_demo);
		
		//初始化ActionMode，请将当前Activity中的Toolbar作为参数传递进去
		setupActionModeWithDecor(getToolbar());
		/*
		 * 设置ActionMode事件监听
		 */
		setActionModeListener(new ActionModeListener() {
			
			@Override
			public void onActionModeShow(ActionMode actionMode) {
				//ActionMode显示的时候
				Log.e("huliang" , "onActionModeShow");
			}
			
			@Override
			public void onActionModeDismiss(ActionMode actionMode) {
				// ActionMode隐藏的时候
				Log.e("huliang" , "onActionModeDismiss");
			}
			
			@Override
			public void onActionItemClicked(Item item) {
				// 在这里处理ActionMode的Item的点击事件
				switch (item.getItemId()) {
				case ActionMode.NAGATIVE_BUTTON:
					enterEditMode(false);
					break;
				case ActionMode.POSITIVE_BUTTON:
					mSelectAll = !mSelectAll;
					if(mSelectAll){
						getActionMode().setPositiveText(getResources().getString(R.string.un_select_all));
					}else{
						getActionMode().setPositiveText(getResources().getString(R.string.select_all));
					}
					break;

				default:
					break;
				}
			}
		});

	}
	
	@Override
	protected void initialWindowParams(Window window) {
		//如果需要在onCreate方法中设置window的属性，请在这里设置
		//例如，window.requestFeature(Window.FEATURE_NO_TITLE);等
		
	}
	
	
	
	@Override
	public void onNavigationClicked(View view) {
		//在这里处理Toolbar上的返回按钮的点击事件
		
		
		onBackPressed();
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(mEditMode){
			enterEditMode(false);
			return;
		}
		super.onBackPressed();
	}
	
	
	
	@Override
	public boolean onMenuItemClick(MenuItem item) {
		//在这里处理Toolbar上的Menu的点击事件
		switch (item.getItemId()) {
		case R.id.menu_info_details2:
			Toast.makeText(this, "Menu Item "+item.getItemId()+"  Clicked", Toast.LENGTH_SHORT).show();
			break;

		default:
			break;
		}
		return super.onMenuItemClick(item);
	}
	
	
	
	
	private void setListAdapter(ListAdapter adapter){
		mListView.setAdapter(adapter);
	}
	
	private ListView getListView(){
		return mListView;
	}

	

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		boolean itemSelected = mSelectedItems.get(position);
		mSelectedItems.put(position, !itemSelected);
		mAdapter.notifyDataSetChanged();
	}


	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		enterEditMode(true);
		
		return false;
	}
	
	private void enterEditMode(boolean editMode){
		mEditMode = editMode;
		
		showActionMode(editMode);
		mBottombar.setVisibility(editMode?View.VISIBLE:View.GONE);
		mAdapter.notifyDataSetChanged();
	}
	
	class MyListAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mItems.length;
		}

		
		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mItems[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = getLayoutInflater().inflate(com.mst.R.layout.list_item_1_line_multiple_choice, null);
			TextView text = (TextView) convertView.findViewById(android.R.id.text1);
			CheckBox box = (CheckBox)convertView.findViewById(android.R.id.button1);
			box.setVisibility(mEditMode?View.VISIBLE:View.GONE);
			text.setText(mItems[position]);
			box.setChecked(mSelectedItems.get(position));
			return convertView;
		}
		
	}
	
	
	
	
}
