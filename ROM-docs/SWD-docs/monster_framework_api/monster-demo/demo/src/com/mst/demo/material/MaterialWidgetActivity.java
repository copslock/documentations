package com.mst.demo.material;

import com.mst.demo.util.Actions;

import mst.app.MstActivity;
import mst.app.MstListActivity;
import mst.widget.MstListView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.mst.demo.R;
public class MaterialWidgetActivity extends MstActivity implements OnItemClickListener{
	private MstListView mListView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setMstContentView(R.layout.list_content);
		mListView = (MstListView) findViewById(android.R.id.list);
		setListAdapter(new ArrayAdapter<String>(this, com.mst.R.layout.list_item_1_line,Actions.MATERIAL_WIDGET));
		getListView().setOnItemClickListener(this);
		getToolbar().setTitle("Material Widget");
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, MaterialDemoActivity.class);
		intent.putExtra(Actions.MATERIAL_WIDGET_ITEMS_POSITION, position);
		intent.putExtra(Actions.MATERIAL_WIDGET_ITEMS_NAME, Actions.MATERIAL_WIDGET[position]);
		startActivity(intent);
	}
	
	public void setListAdapter(ListAdapter adapter){
		mListView.setAdapter(adapter);
	}
	
	public ListView getListView(){
		return mListView;
	}
	

}
