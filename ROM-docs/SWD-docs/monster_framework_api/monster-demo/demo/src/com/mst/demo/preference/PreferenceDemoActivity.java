package com.mst.demo.preference;

import mst.app.MstActivity;

import com.mst.demo.util.FragmentUtils;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class PreferenceDemoActivity extends MstActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		FragmentUtils u = new FragmentUtils(this);
		u.switchToFragment(PreferenceDemoFragment.class.getName(), 
				com.mst.R.id.content,null, false, false, 0, null, false);
		setTitle("Preference");
	}
	
	

}
