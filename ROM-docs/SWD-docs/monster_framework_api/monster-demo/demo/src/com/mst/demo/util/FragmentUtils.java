package com.mst.demo.util;

import com.mst.demo.material.DatePickerFragment;
import com.mst.demo.material.FloatingActionButtonFragment;
import com.mst.demo.material.MenuFragment;
import com.mst.demo.material.NumberPickerFragment;
import com.mst.demo.material.RecyclerViewFragment;
import com.mst.demo.material.SliderLayoutFragment;
import com.mst.demo.material.SpinnerFragment;
import com.mst.demo.material.TabFragment;
import com.mst.demo.material.TimePickerFragment;
import com.mst.demo.material.ToolbarFragment;
import com.mst.demo.material.CompoundButtonFragment;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.ViewGroup;


public class FragmentUtils {
	
	private Activity mActivity;
	public FragmentUtils(Activity context){
		this.mActivity = context;
	}
	
	public static final String[] ENTRY_FRAGMENTS = {
		ToolbarFragment.class.getName(),
		FloatingActionButtonFragment.class.getName(),
		SliderLayoutFragment.class.getName(),
		RecyclerViewFragment.class.getName(),
		TabFragment.class.getName(),
		NumberPickerFragment.class.getName(),
		TimePickerFragment.class.getName(),
		DatePickerFragment.class.getName(),
		MenuFragment.class.getName(),
		SpinnerFragment.class.getName(),
		CompoundButtonFragment.class.getName()
	};
	
	
    public boolean isValidFragment(String fragmentName) {
        // Almost all fragments are wrapped in this,
        // except for a few that have their own activities.
        for (int i = 0; i < ENTRY_FRAGMENTS.length; i++) {
            if (ENTRY_FRAGMENTS[i].equals(fragmentName)) return true;
        }
        return false;
    }
    
    public Fragment switchToFragment(String fragmentName, Bundle args, boolean validate,
            boolean addToBackStack, int titleResId, CharSequence title, boolean withTransition) {
     return switchToFragment(fragmentName, com.mst.demo.R.id.content, args, validate, addToBackStack, titleResId, title, withTransition);
    }
    
    
    public Fragment switchToFragment(String fragmentName, int contentViewId,Bundle args, boolean validate,
            boolean addToBackStack, int titleResId, CharSequence title, boolean withTransition) {
        if (validate && !isValidFragment(fragmentName)) {
            throw new IllegalArgumentException("Invalid fragment for this activity: "
                    + fragmentName);
        }
        Fragment f = Fragment.instantiate(mActivity, fragmentName, args);
        FragmentTransaction transaction = mActivity.getFragmentManager().beginTransaction();
        transaction.replace(contentViewId, f);
        ViewGroup content = (ViewGroup)mActivity.findViewById(contentViewId);
        if (withTransition) {
            TransitionManager.beginDelayedTransition(content);
        }
        if (addToBackStack) {
            transaction.addToBackStack(":monster_demo:prefs");
        }
        if (titleResId > 0) {
            transaction.setBreadCrumbTitle(titleResId);
        } else if (title != null) {
            transaction.setBreadCrumbTitle(title);
        }
        transaction.commitAllowingStateLoss();
        mActivity.getFragmentManager().executePendingTransactions();
        return f;
    }
    
    

}
