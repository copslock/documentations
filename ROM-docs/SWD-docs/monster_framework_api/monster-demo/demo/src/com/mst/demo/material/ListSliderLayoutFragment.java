package com.mst.demo.material;

import mst.widget.MstListView;
import mst.widget.toolbar.Toolbar;
import mst.widget.toolbar.Toolbar.OnMenuItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.mst.demo.R;
import com.mst.demo.util.FragmentUtils;
public class ListSliderLayoutFragment extends Fragment {
	private static final String 	TAG = "SliderLayoutFragment";
	/**
	 * ListView 必须是MstListView，否则触摸事件会冲突
	 */
	private MstListView mListView;
	
    private String[] mItems;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mListView= (MstListView) inflater.inflate(R.layout.list_content, container,false);
		mItems = getResources().getStringArray(R.array.list_test_array);
		mListView.setAdapter(new MyAdapter());
		return mListView;
	}

	/**
	 *Adapter和平时使用ListView时的Adapter一样 
	 *
	 */
	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mItems.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mItems[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_with_slider_layout, null);
			TextView title = (TextView) convertView.findViewById(android.R.id.text1);
			title.setText(mItems[position]);
			return convertView;
		}
		
	}

	

}
