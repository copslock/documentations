package com.mst.demo.material;

import java.util.ArrayList;
import java.util.List;

import com.mst.demo.AgendaFragment;
import com.mst.demo.FragmentAdapter;
import com.mst.demo.InfoDetailsFragment;
import com.mst.demo.ShareFragment;

import mst.widget.DrawerLayout;
import mst.widget.NavigationView;
import mst.widget.NumberPicker;
import mst.widget.NumberPicker.OnValueChangeListener;
import mst.widget.ViewPager;
import mst.widget.tab.TabLayout;
import mst.widget.toolbar.Toolbar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mst.demo.R;
public class NumberPickerFragment extends Fragment {
	private NumberPicker mPicker ,mStringPicker;
	private TextView mResult;
	private String[] mStringValues;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_number_picker, container,false);
		initView(view);
		return view;
	}
	

    private void initView(View view) {
    	mResult = (TextView)view.findViewById(android.R.id.text1);
    	mPicker = (NumberPicker)view.findViewById(R.id.number_picker);
    	mStringPicker = (NumberPicker)view.findViewById(R.id.number_picker2);
    	mStringValues = getResources().getStringArray(R.array.items_entry);
    	/*
    	 * 如果需要显示字符串，首先需要设置最小值为0，最大值为字符串数组的长度
    	 * 减 1，然后调用public void setDisplayedValues(String[] displayedValues)
    	 * 方法将需要显示的数组传递进去
    	 */
    	mStringPicker.setMinValue(0);
    	mStringPicker.setMaxValue(mStringValues.length -1);
    	mStringPicker.setDisplayedValues(mStringValues);
    	/*
    	 * 如果只显示数字，直接设置数字返回即可
    	 */
    	mPicker.setMinValue(0);
    	mPicker.setMaxValue(100);
    	mResult.setText(mPicker.getValue()+"");
    	//设置NumberPicker选择监听
    	mPicker.setOnValueChangedListener(new OnValueChangeListener() {
			
			@Override
			public void onValueChange(NumberPicker picker, int oldValue, int newValue) {
				// TODO Auto-generated method stub
				mResult.setText(picker.getValue()+"");
			}
		});
    	
    	//设置NumberPicker选择监听
    	mStringPicker.setOnValueChangedListener(new OnValueChangeListener() {
			
			@Override
			public void onValueChange(NumberPicker picker, int oldValue, int newValue) {
				// TODO Auto-generated method stub
				mResult.setText(mStringValues[newValue]);
			}
		});
    }
    
    

}
