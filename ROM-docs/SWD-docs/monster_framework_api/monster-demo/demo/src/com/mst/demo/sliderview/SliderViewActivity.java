package com.mst.demo.sliderview;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import mst.widget.DatePicker;
import mst.widget.MstListView;

import mst.app.MstActivity;
import android.graphics.Color;

import com.mst.demo.R;
import mst.widget.SliderView;

/**
 * Created by caizhongting on 16-9-1.
 */
public class SliderViewActivity extends MstActivity implements SliderView.OnSliderButtonLickListener{

    SliderView view1;
    SliderView view2;
    SliderView view3;

    TextView text3;

    MstListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_slider_view);
        listview = (MstListView) findViewById(R.id.listview);
//        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(SliderViewActivity.this, "onClick : "+position, Toast.LENGTH_SHORT).show();
//            }
//        });
//        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(SliderViewActivity.this, "onLongClick : "+position, Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        });

        listview.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return 5;
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater layoutInflater = LayoutInflater.from(SliderViewActivity.this);
                SliderView sliderView = (SliderView) layoutInflater.inflate(R.layout.slider_view,null);
                sliderView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
                sliderView.addTextButton(1,"隐藏");
                sliderView.addTextButton(2,"忽略");
                sliderView.addTextButton(3,"删除");
                sliderView.setButtonBackgroundColor(0, Color.parseColor("#ffb3b3b3"));
                sliderView.setButtonBackgroundColor(1, Color.parseColor("#ff7691d8"));
                sliderView.setOnSliderButtonClickListener(SliderViewActivity.this);
                sliderView.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        android.util.Log.e("test","test onclick");
                        Toast.makeText(SliderViewActivity.this,"test onclick",Toast.LENGTH_SHORT).show();
                    }
                });
                sliderView.setOnLongClickListener(new View.OnLongClickListener(){
                    @Override
                    public boolean onLongClick(View v) {
                        android.util.Log.e("test","test onLongClick");
                        Toast.makeText(SliderViewActivity.this,"test onLongClick",Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });
                sliderView.setCustomBackground(SliderView.CUSTOM_BACKGROUND_RIPPLE);
                return sliderView;
            }
        });

//        view1 = (SliderView) findViewById(R.id.slider_view1);
//        view2 = (SliderView) findViewById(R.id.slider_view2);
//        view3 = (SliderView) findViewById(R.id.slider_view3);
//
//        view1.addTextButton(1,"隐藏");
//        view1.addTextButton(2,"忽略");
//        view1.addTextButton(3,"删除");
//        view1.setOnSliderButtonClickListener(this);
//        view1.setButtonBackgroundColor(0, Color.parseColor("#ffb3b3b3"));
//        view1.setButtonBackgroundColor(1, Color.parseColor("#ff7691d8"));
//
//        view2.addTextButton(1,"隐藏");
//        view2.addTextButton(2,"删除");
//        view2.setOnSliderButtonClickListener(this);
//        view2.setButtonBackgroundColor(0, Color.parseColor("#ffb3b3b3"));
//
//
//        view3.addTextButton(1,"隐藏");
//        view3.addTextButton(2,"忽略");
//        view3.setOnSliderButtonClickListener(this);
//        view3.setButtonBackgroundColor(0, Color.parseColor("#ffb3b3b3"));
//        view3.setButtonBackgroundColor(1, Color.parseColor("#ff7691d8"));
//
//        text3 = (TextView) view3.findViewById(R.id.contact_name);


    }

    @Override
    public void onSliderButtonClick(int id, View view, ViewGroup parent) {
        switch (id) {
            case 1:
                Toast.makeText(this, "view1 : 1", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "view1 : 2", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(this, "view1 : 3", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
